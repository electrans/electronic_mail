# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import template
from . import purchase


def register():
    Pool.register(
        purchase.Purchase,
        template.Template,
        module='electrans_electronic_mail', type_='model')
