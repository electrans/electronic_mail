from trytond.pool import Pool, PoolMeta


class Template(metaclass=PoolMeta):
    __name__ = 'electronic.mail.template'

    @classmethod
    def send_mail(cls, mail_id, template=False):
        """
        Override function to mark purchase as Sended into purchase.purchase
        """
        mail = super(Template, cls).send_mail(mail_id, template)
        if mail and mail_id.message_id.startswith('purchase.purchase'):
            # Mark purchase as Sended
            purchase = Pool().get('purchase.purchase').search([
                ('id', '=', int(mail_id.message_id.split(',')[1]))
            ])
            if purchase:
                purchase[0].mail_sended = True
                purchase[0].save()
        return mail