from trytond.pool import PoolMeta
from trytond.model import ModelView, fields
from trytond.exceptions import UserError
from trytond.i18n import gettext

__all__ = ['Purchase']


class Purchase(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'

    mail_sended = fields.Boolean("Mail sended", select=False,
                                     help = "If this is marked, the purchase has been by mail sent to the supplier")

    @classmethod
    @ModelView.button
    def process(cls, purchases):
        for purchase in purchases:
            if not purchase.mail_sended:
                raise UserError(gettext('electrans_electronic_mail.no_mail_sended'))
        super(Purchase, cls).process(purchases)
